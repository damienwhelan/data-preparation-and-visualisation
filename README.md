# Data Preparation and Visualisation


## Exploratory data analysis
    • Graphical methods - box plot s, scatter plots, heat maps
    • Types of missing dat a. Handling missing data
    • Data quality/ cleaning data/ dirty data concepts
    • Handling out-of-range values

## Feature selection
    • Filter, Wrapper and Embedded approaches
    • Embedded feature selection methods - Regulari sati on, Decision Tree
    • Subset selection - Stepwise forward selection, Stepwise backward elimination, all subsets
    • Information theoretical methods - Minimum-redundancy, maximum-relevance

## Dimensionality reduction
    • Principal Components Analysis
    • Independent Components Analysis
    • Non-negative Matrix Factorisation
    • Local Linear Embedding
    • Self-organising maps
    • Restricted Boltzmann Machine and Deep Belief Networks

## Fundamentals of data visualisation
    • What is Data Visualisation?
    • Characteristics of data, data types and information
    • The benefits of communicating data using visualisation
    • Tools for data visualisation

## Visualisation techniques
    • Exploratory vs Explanatory visualisation
    • Taxonomy of visualisation techniques - visualising relationships, comparisons, composition, distributions, and hierarchical data
    • Visual variables and visual ratios
    • Colour in data visualisation
    • Annotation (practical

## Principles of data visualisation
    • Trustworthiness
    • Integrity
    • Accessibility
    • Tufte's and Kirk's design principles

## The visualisation process
    • Formulating a brief
    • Working with the data
    • Establishing an editorial mindset
    • Developing a visualisation solution

## Interactive visualisation
    • Benefits of interactive visualisation
    • Framing, navigating, animating and sequencing
    • Brushing, Linking, Filtering, Zoom
